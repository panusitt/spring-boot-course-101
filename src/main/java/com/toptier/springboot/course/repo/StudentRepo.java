package com.toptier.springboot.course.repo;

import com.toptier.springboot.course.entity.StudentEntity;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends CrudRepository<StudentEntity, Integer> {

  // Entity <---> DTO
  // Entity >>> Business Layer
  // DTO >>> public zone Layer (request | response)

  @Modifying
  @Query("""
          insert into student_moss (code,
                                    name,
                                    last_name,
                                    level,
                                    date_of_birth,
                                    sex,
                                    email,
                                    age)
          values (:code,
                  :name,
                  :last_name,
                  :level,
                  :date_of_birth,
                  :sex,
                  :email,
                  :age)
          """)
  int insertStudent(@Param("code") String code,
                    @Param("name") String name,
                    @Param("last_name") String lastName,
                    @Param("level") String level,
                    @Param("date_of_birth") String dateOfBirth,
                    @Param("sex") String sex,
                    @Param("email") String email,
                    @Param("age") int age);

  @Query("select * from student_moss")
  List<StudentEntity> findAllStudents();

  @Query("select * from student_moss where code=:code")
  StudentEntity findOneByStudentCode(@Param("code") String code);

  @Modifying
  @Query("update student_moss set name=:name, last_name=:last_name where code=:code")
  int updateNameByStudentCode(@Param("name") String name,
                              @Param("last_name") String lastName,
                              @Param("code") String code);

  @Modifying
  @Query("delete from student_moss where code = :code")
  int deleteByStudentCode(@Param("code") String code);
}
