package com.toptier.springboot.course.controller.interceptor;

import com.toptier.springboot.course.service.AppPermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Log
@Component
@RequiredArgsConstructor
public class AppIdInterceptor implements HandlerInterceptor {

  final AppPermissionService appPermissionService;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
//    if (appPermissionService.hasPermission(request.getHeader("X-App-Id"))) {
//      return true;
//    } else {
//      throw new UnAuthException("no permissions, please contact the system administrator.");
//    }
    return true;
  }
}
