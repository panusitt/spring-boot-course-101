package com.toptier.springboot.course.service;

import com.toptier.springboot.course.dto.Student;
import com.toptier.springboot.course.dto.advice.BusinessException;
import com.toptier.springboot.course.entity.StudentEntity;
import com.toptier.springboot.course.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

  final StudentRepo repo;

  // create student
  public String createStudent(Student request) {

    int insert = repo.insertStudent(
            request.getCode(),
            request.getName(),
            request.getLastName(),
            request.getLevel(),
            request.getDateOfBirth(),
            request.getSex(),
            request.getEmail(),
            request.getAge()
    );
    if (insert < 1) {
      return "create student unsuccess!!";
    }
    return "created";
    // return insert < 1 ? "create student unsuccess!!" : "created";
  }

  // find all student
  public List<Student> findAllStudents() {
    List<Student> students = new ArrayList<>();
    List<StudentEntity> studentEntity = repo.findAllStudents();

    // business - logic

    // using by foreach
    studentEntity.forEach(val -> {
      Student student = new Student();
      student.setName(val.getName());
      student.setLastName(val.getLastName());
      student.setEmail(val.getEmail());
      students.add(student);
    });

    // using by for
//    for (int i = 0; i < studentEntity.size(); i++) {
//      Student student = new Student();
//      student.setName(studentEntity.get(i).getName());
//      student.setLastName(studentEntity.get(i).getLastName());
//      student.setEmail(studentEntity.get(i).getEmail());
//      students.add(student);
//    }
    return students;
  }

  // find by student code
  public Student findStudentByCode(String code) {
    if (null == code || "".equals(code)) {
      throw new BusinessException(400, "student code cannot empty or null");
    }
    // Entity
    StudentEntity studentEntity = repo.findOneByStudentCode(code);
    if (null == studentEntity) {
      throw new BusinessException(400, "student code: " + code + " not found.");
    }
    // DTO
    Student student = new Student();
    student.setName(studentEntity.getName());
    student.setLastName(studentEntity.getLastName());
    student.setEmail(studentEntity.getEmail());
    return student;
  }

  // update student
  public String updateStudent(Student request) {
    if (null == request.getCode() || "".equals(request.getCode())) {
      throw new BusinessException(400, "student code cannot empty or null");
    }
    int update = repo.updateNameByStudentCode(
            request.getName(),
            request.getLastName(),
            request.getCode()
    );
    if (update < 1) {
      throw new RuntimeException("student code: " + request.getCode() + " update fail!!");
//      return "update student unsuccess!!";
    }
    return "updated";
  }

  // delete student
  public String deleteStudent(String code) {
    if (null == code || "".equals(code)) {
      throw new BusinessException(400, "student code cannot empty or null");
    }
    int delete = repo.deleteByStudentCode(code);
    if (delete < 1) {
      throw new RuntimeException("student code: " + code + " delete fail!!");
//      return "delete student fail!!";
    }
    return "deleted";
  }

}
