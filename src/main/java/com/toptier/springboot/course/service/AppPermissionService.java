package com.toptier.springboot.course.service;

import com.toptier.springboot.course.repo.AppPermissionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AppPermissionService {

  final AppPermissionRepo repo;

  public boolean hasPermission(String appId) {
    return null != repo.findOneByAppId(appId);
  }
}
