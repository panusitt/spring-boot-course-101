package com.toptier.springboot.course.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Student {

  private String code;
  private String name;
  private String lastName;
  private String level; // Enum
  private String dateOfBirth;
  private String sex;
  private String email;
  @JsonIgnore
  private Integer age;
  private List<Course> courses = new ArrayList<>(0);

  // yyyy/MM/dd
  public void setDateOfBirth(String dateOfBirth) {
    setAge(
            Long.valueOf(
                    ChronoUnit.YEARS.between(
                            LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("yyyy/MM/dd")),
                            LocalDateTime.now()
                    )
            ).intValue()
    );
    this.dateOfBirth = dateOfBirth;
  }
}
