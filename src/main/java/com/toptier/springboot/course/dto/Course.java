package com.toptier.springboot.course.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Course {

  private String code;
  private String name;
  private Double price;

}
